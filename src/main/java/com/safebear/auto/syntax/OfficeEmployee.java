package com.safebear.auto.syntax;

public class OfficeEmployee extends Employee {
    private boolean desk;

    public void newDesk(boolean newDesk) {
        desk = newDesk;
    }
    public boolean getDesk(){
        return desk;
    }
    public void setDesk(boolean desk){
        this.desk = desk;
    }
}
