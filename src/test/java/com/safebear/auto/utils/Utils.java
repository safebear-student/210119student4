package com.safebear.auto.utils;

import com.safebear.auto.nonBddTests.LoginTests;
import freemarker.template.SimpleDate;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static String getUrl(){
        return URL;

    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/iedriver.exe");
        System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/edgedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

            case "firefox":
                return new FirefoxDriver();

            case "internet explorer:":
                return new InternetExplorerDriver();

            case "edge":
                return new EdgeDriver();

            default:
                return new ChromeDriver(options);
        }
    }
    public static String generateScreenShotFileName(){
        //creates the file name for the screenshots
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())+".png";
    }
    public static void captureScreenshot(WebDriver driver, String fileName){
        // takes the screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        // make sure the screenshots directory exists
        File file = new File("target/screenshots");
        if (!file.exists()){
            if (file.mkdir()){
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create Directory!");
            }

        }
        // Copy file to filename and location we set before
        try {
            copy(scrFile, new File("target/screenshots/"+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
