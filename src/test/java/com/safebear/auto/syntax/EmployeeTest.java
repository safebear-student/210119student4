package com.safebear.auto.syntax;

import org.testng.annotations.Test;

@Test
public class EmployeeTest {
    public void testEmployee(){
        //This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();

        //Creating a sales employee object
        SalesEmployee victoria = new SalesEmployee();

        //Creating an office employee object
        OfficeEmployee derrick = new OfficeEmployee();

        //this is where we employ Hannah and fire Bob
        hannah.employ();
        bob.fire();

        //This is where we employ Victoria and give her a car
        victoria.employ();
        victoria.changeCar("BMW");

        //This is where we employe Derrick and give him a desk
        derrick.employ();
        derrick.newDesk(true);

        //Let'sprint their state to the screen
        System.out.println("Hannah's employment state: " + hannah.isEmployed());
        System.out.println("Bob's employment state: " + bob.isEmployed());
        System.out.println("Victoria's employment state: " + victoria.isEmployed());
        System.out.println("Victoria's car is: " + victoria.getCar());
        System.out.println("Derrick's employment state: " + derrick.isEmployed());
        System.out.println("Does Derrick have a desk: " + derrick.getDesk());
    }
}
