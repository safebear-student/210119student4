package com.safebear.auto.nonBddTests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void validLogin(){
        // Go to the login page
        driver.get(Utils.getUrl());
        // Confirm you're on the login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Login as a valid user
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        // Check you're on the Tools Page
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page");
        // Confirm the login successful message (contains 'Success') appears
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Success"));
        //Generates the screenshot, puts it in target/screenshots
        Utils.captureScreenshot(driver, Utils.generateScreenShotFileName());
    }
    @Test
    public void invalidLogin(){
        // Go to te login page
        driver.get(Utils.getUrl());
        // Check you're on the login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Enter invalid username AND password
        loginPage.enterUsername("THIS SHOULD FAIL");
        loginPage.enterPassword("howdy");
        loginPage.clickLoginButton();
        // Check you're still on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page");
        // Check the failed login message (contains 'incorrect') appears
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("incorrect"));
        //Generates the screenshot, puts it in target/screenshots
        Utils.captureScreenshot(driver, Utils.generateScreenShotFileName());
    }
    @Test
    public void invalidUsername(){
        // Go to te login page
        driver.get(Utils.getUrl());
        // Check you're on the login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Enter invalid username, but valid password
        loginPage.enterUsername("THIS SHOULD FAIL");
        loginPage.enterPassword("letmin");
        loginPage.clickLoginButton();
        // Check you're still on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page");
        // Check the failed login message (contains 'incorrect') appears
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("incorrect"));
        //Generates the screenshot, puts it in target/screenshots
        Utils.captureScreenshot(driver, Utils.generateScreenShotFileName());
    }
    @Test
    public void invalidPassword(){
        // Go to te login page
        driver.get(Utils.getUrl());
        // Check you're on the login page
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        // Enter valid username but invalid password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("howdy");
        loginPage.clickLoginButton();
        // Check you're still on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page");
        // Check the failed login message (contains 'incorrect') appears
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("incorrect"));
        //Generates the screenshot, puts it in target/screenshots
        Utils.captureScreenshot(driver, Utils.generateScreenShotFileName());
    }
}
